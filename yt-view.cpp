#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include "utf8LogHandler.h"

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    qInstallMessageHandler(utf8LogHandler);
    qDebug() << "Hello World!";
    MainWindow w;
    w.show();
    return app.exec();
}
