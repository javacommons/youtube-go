#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtCore>
#include "installer.h"
#include <cmath>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->listWidget->setViewMode(QListWidget::IconMode);
    ui->listWidget->setIconSize(QSize(400,400));
    ui->listWidget->setResizeMode(QListWidget::Adjust);
    ui->listWidget->setMovement(QListWidget::Static);
    this->on_actionTest01_triggered();
}

MainWindow::~MainWindow()
{
    delete ui;
}


//namespace {

const QString ELLIPSIS("...");

//}

QString limitString(const QString& aString, int maxLength)
{
    if (aString.length() <= maxLength)
        return aString;
    float spacePerPart = (maxLength - ELLIPSIS.length()) / 2.0;
    auto beforeEllipsis = aString.left(std::ceil(spacePerPart));
    auto afterEllipsis = aString.right(std::floor(spacePerPart));

    return beforeEllipsis + ELLIPSIS + afterEllipsis;
}

void MainWindow::on_actionTest01_triggered()
{
    qDebug() << "MainWindow::on_actionTest01_triggered()";
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    qDebug() << env.value("YOUTUBE_API_KEY");
    QUrl url("https://www.googleapis.com/youtube/v3/search");
    QUrlQuery query;
    query.addQueryItem("key", env.value("YOUTUBE_API_KEY"));
    query.addQueryItem("q", "洋楽");
    query.addQueryItem("part", "snippet, id");
    query.addQueryItem("maxResults", "10");
    url.setQuery(query.query());
    qDebug() << url;
    NetworkManager nm;
    QString result = downloadText(nm, url.toString());
    qDebug().noquote() << result;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(result.toUtf8());
    QVariantMap map = jsonDoc.toVariant().toMap();
    QVariantList list = map["items"].toList();
    qDebug() << list.size();
    ui->listWidget->clear();
    for(int i=0; i<list.size(); i++)
    {
        QVariantMap item = list[i].toMap();
        QString kind = item["id"].toMap()["kind"].toString();
        if(kind != "youtube#video") continue;
        QString videoId = item["id"].toMap()["videoId"].toString();
        qDebug() << videoId;
        QVariantMap snippet = item["snippet"].toMap();
        QString title = snippet["title"].toString();
        //QString urlString = snippet["thumbnails"].toMap()["default"].toMap()["url"].toString();
        QString urlString = snippet["thumbnails"].toMap()["high"].toMap()["url"].toString();
        QUrl url(urlString);
        QNetworkReply* reply = nm.get(QNetworkRequest(url));
        if (reply->error() == QNetworkReply::NoError)
        {
            QImageReader imageReader(reply);
            QImage pic = imageReader.read();
            QIcon icon(QPixmap::fromImage(pic));
            QListWidgetItem *lwItem = new QListWidgetItem(icon, limitString(title, 75), ui->listWidget);
            lwItem->setToolTip(title);
            ui->listWidget->addItem(lwItem);
         }
    }
}

